package ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import ru.nsu.fit.shelestova.j2ee.task1.jdbc.PostgersqlConnector;

public class StatementInserter extends Inserter {
	 private static final String insertQuery = "INSERT INTO " + PostgersqlConnector.TABLE_NAME + "  (name, creation_date) " +  
	            "VALUES('%s', NOW())";
	 private Statement st;
	 
	public StatementInserter(Connection db, int rowsNumber) throws SQLException {
		super(db, rowsNumber);
		
		st = db.createStatement();
	}

	@Override
	protected void executeQuery(int row) throws SQLException {
		String query = String.format(insertQuery, String.valueOf(row));
		st.execute(query);		
	}

	
}

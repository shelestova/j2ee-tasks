package ru.nsu.fit.shelestova.j2ee.task2.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Message {
	public Message(){}
	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	private User creater;

	@ManyToOne
	private Topic topic;

	@Column(nullable = false)
	private String content;
	
	public Message(String content, Topic topic, User creater){
		this.content = content;
		this.topic = topic;
		this.creater = creater;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", creater=" + creater + ", topic="
				+ topic + ", content=" + content + "]";
	}

	
}

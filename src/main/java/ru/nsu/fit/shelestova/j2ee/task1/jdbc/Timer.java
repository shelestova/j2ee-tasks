package ru.nsu.fit.shelestova.j2ee.task1.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters.Inserter;

public class Timer {
	private static final Logger log = LoggerFactory.getLogger(Timer.class);

	public static long run(Inserter inserter) {
		log.info("{} started...", inserter);
		long startTime = System.currentTimeMillis();

		inserter.run();

		log.info("{} finished.", inserter);
		long stopTime = System.currentTimeMillis();
		
		long time = stopTime - startTime;
		log.info("{} worked for {} millis", inserter, time);

		return time;
	}

}

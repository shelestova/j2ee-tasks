package ru.nsu.fit.shelestova.j2ee.task3.servlet;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.User;

@SuppressWarnings("serial")
public class Servlet extends HttpServlet {
	private static Logger log = LoggerFactory.getLogger(Servlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userParam = request.getParameter("user");

		if (userParam == null) {
			log.error("user = null.");
			PrintWriter out = response.getWriter();
	        out.println("User parameter \'user\' in uri for user id");
			return;
		}

		Long userId = null;
		try {
			userId = Long.parseLong(userParam);
			log.info("ID " + userId);
		} catch (NumberFormatException e) {
			log.error("inputed userID is not number.", e);
			return;
		}

		
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("j2ee-tasks");
		EntityManager em = emf.createEntityManager();
		
		
		try {
			Query userQuery = em
					.createQuery("SELECT user from User user where user.id = :userId");
			userQuery.setParameter("userId", userId);
			User user = (User) userQuery.getSingleResult();

			if (user != null) {

				String email = user.getEmail();

				BufferedImage emailImage = new BufferedImage(500, 50,
						BufferedImage.TYPE_INT_ARGB);
				Graphics g = emailImage.getGraphics();
				Font font = new Font("Serif", Font.PLAIN, 14);
				g.setFont(font);
				g.setColor(Color.black);
				g.drawString(email, 20, 30);

				response.setContentType("image/png");
				OutputStream os = response.getOutputStream();
				ImageIO.write(emailImage, "png", os);
				os.close();
			} else {

				PrintWriter out = response.getWriter();
		        out.println("User with id " + userId
						+ " not found");

			}
		} finally {
			em.close();
		}
	}

}

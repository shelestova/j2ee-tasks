package ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ru.nsu.fit.shelestova.j2ee.task1.jdbc.PostgersqlConnector;

public class PreparedBatchInserter extends Inserter {
	private static final String insertQuery = "INSERT INTO "
			+ PostgersqlConnector.TABLE_NAME + " (name, creation_date) "
			+ "VALUES(?, NOW())";

	private PreparedStatement st;

	public PreparedBatchInserter(Connection db, int rowsNumber)
			throws SQLException {
		super(db, rowsNumber);

		st = db.prepareStatement(insertQuery);
	}
	
	@Override
	protected void executeQuery(int row) throws SQLException {
		st.setString(1, String.valueOf(row));
		st.addBatch();
	}

	@Override
	protected void commit() throws SQLException {
		st.executeBatch();
		db.commit();
	}

}

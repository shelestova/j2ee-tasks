package ru.nsu.fit.shelestova.j2ee.task2.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Topic {
	public Topic(){}
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(unique=true, nullable = false)
    private String name;
	
    @ManyToOne
    private User creater;
    
    public Topic(String name, User creater){
    	this.name = name;
    	this.creater = creater;
    }
    
    @Override
	public String toString() {
		return "Topic [id=" + id + ", name=" + name + ", creater=" + creater
				+ "]";
	}

}

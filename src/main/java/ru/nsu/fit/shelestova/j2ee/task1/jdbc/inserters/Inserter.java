package ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class Inserter implements Runnable {
	protected Connection db;
	protected int rowsNumber;

	public Inserter(Connection db, int rowsNumber) throws SQLException {
		this.db = db;
		this.rowsNumber = rowsNumber;

		db.setAutoCommit(false);
	}

	public void run() {
		try {
			for (int times = 0; times < rowsNumber; times++) {
				executeQuery(times);
			}

			commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void commit() throws SQLException{
		db.commit();
	}

	protected abstract void executeQuery(int row) throws SQLException;
	
	@Override
	public String toString(){
		return getClass().getSimpleName() + " : for " + rowsNumber + " rows";
	}

}

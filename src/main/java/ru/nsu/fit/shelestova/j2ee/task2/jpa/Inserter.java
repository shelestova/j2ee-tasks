package ru.nsu.fit.shelestova.j2ee.task2.jpa;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.Admin;
import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.Message;
import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.Topic;
import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.User;

public class Inserter {
	private static Logger log = LoggerFactory.getLogger(Inserter.class);
	
	public static void insert(EntityManager em) {
		log.info("Inserting started...");
		
		for(int userNumber = 0; userNumber < 10; userNumber++){
			String userName = "user #" + userNumber;
			
			User user = new User(userName, userName + "@someDN");
			em.persist(user);
			
			if( userNumber%2 == 0){
				int topicNumber = userNumber / 2;
				Topic topic = new Topic("Topic #" + topicNumber, user);
				em.persist(topic);
				
				if(topicNumber %2 == 0){
					for(int contentNumber = 0; contentNumber < 3; contentNumber++){
						Message message = new Message("some content #" + contentNumber, topic, user);
						em.persist(message);
					}
				}
			}
		}
		
		for (int adminNumber = 0; adminNumber < 3; adminNumber++){
			String adminName = "admin #" + adminNumber;
			Admin admin = new Admin(adminName, adminName + "@someDN");
			em.persist(admin);
		}
		
		log.info("Inserting finished.");
	}

}

package ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ru.nsu.fit.shelestova.j2ee.task1.jdbc.PostgersqlConnector;

public class PreparedInserter extends Inserter {
	private static final String insertQuery = "INSERT INTO "
			+ PostgersqlConnector.TABLE_NAME + " (name, creation_date) "
			+ "VALUES(?, NOW())";

	private PreparedStatement st;

	public PreparedInserter(Connection db, int rowsNumber) throws SQLException {
		super(db, rowsNumber);

		st = db.prepareStatement(insertQuery);
	}

	@Override
	protected void executeQuery(int row) throws SQLException {
		st.setString(1, String.valueOf(row));
		st.executeUpdate();
	}

}

package ru.nsu.fit.shelestova.j2ee.task2.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Admin extends User{

	public Admin(){}
	public Admin(String name, String email) {
		super(name, email);
	}


	@Override
	public String toString() {
		return "Admin ::" + super.toString();
	}
}

package ru.nsu.fit.shelestova.j2ee.task2.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity(name="users")
@Table(name = "users")
public class User {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(unique=true, nullable = false)
    private String name;
    
    @Column(nullable=false)
    private String email;
    
    public User(){}
    
    public User(String name, String email){
		this.name = name;
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email= " + email + "]";
	}
	
	public String getEmail(){
		return email;
	}
}

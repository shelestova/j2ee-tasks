package ru.nsu.fit.shelestova.j2ee.task2.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.Admin;
import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.Message;
import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.Topic;
import ru.nsu.fit.shelestova.j2ee.task2.jpa.model.User;

public class Application {
	private static Logger log = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("j2ee-tasks");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		//Inserter.insert(em);
		
		em.getTransaction().commit();
		
		printDB(em);
		
		em.close();
	
	}

	private static void printDB(EntityManager em) {
		Query userQuery = em.createQuery("SELECT user from User user");
        List<?> users = userQuery.getResultList();
        
        log.info("Users:");
        for( Object db_user : users ) {
            User user = (User) db_user;
            System.out.println(user);
        }
        
        log.info("Topics:");
        Query topicQuery = em.createQuery("SELECT topic from Topic as topic inner join topic.creater");
        List<?> topics = topicQuery.getResultList();
        
        System.out.println("Topics: ");
        for( Object db_topic : topics ) {
            Topic topic = (Topic) db_topic;
            System.out.println(topic);
        }
        
        log.info("Messages:");
        Query messageQuery = em.createQuery("SELECT message from Message as message " + 
                "inner join message.creater innser join message.topic");
        List<?> messages = messageQuery.getResultList();
        
        for( Object db_message : messages ) {
            Message message = (Message) db_message;
            System.out.println(message);
        }
		
        log.info("All is printed.");
	}

}

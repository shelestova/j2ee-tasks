package ru.nsu.fit.shelestova.j2ee.task1.jdbc;

import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters.PreparedBatchInserter;
import ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters.PreparedInserter;
import ru.nsu.fit.shelestova.j2ee.task1.jdbc.inserters.StatementInserter;


public class PostgersqlConnector {
	private static final Logger log = LoggerFactory.getLogger(PostgersqlConnector.class);
	
	private static final String url = "jdbc:postgresql://localhost:5432/postgres";
	private static final String username = "postgres";
	private static final String password = "123";
	
	public static final String TABLE_NAME = "SomeTable";
	
	public static void main(String[] args) throws ClassNotFoundException{
		Connection db = null;
		Statement st = null;
		try {
			db = DriverManager.getConnection(url, username, password);	
			st = db.createStatement();
			
			String createTableQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME 
					+ "(id SERIAL, "
					+ "name VARCHAR(50), "
					+ "creation_date TIMESTAMP);";			
			
			st.executeUpdate(createTableQuery);
			
			log.info("Table {} is created", TABLE_NAME);	
			
			int rowsNumber = 1000;
			
			Timer.run(new StatementInserter(db, rowsNumber));
			cleanTable(db);
			
			Timer.run(new PreparedInserter(db, rowsNumber));
			cleanTable(db);
			
			Timer.run(new PreparedBatchInserter(db, rowsNumber));
			cleanTable(db);

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if(db != null){
				try {
					db.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}	
	
	private static void cleanTable(Connection db) throws SQLException {
        Statement statement = db.createStatement();
        statement.executeUpdate("TRUNCATE TABLE " + TABLE_NAME);
        
        log.info("{} is cleaned", TABLE_NAME);
    }
}
